﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinCondition : MonoBehaviour
{

   public SceneChanger changeScene;
   void OnTriggerEnter2D(Collider2D collider){
       if(collider.tag == "Player"){
           changeScene.NextScene();
       }
   }
}
