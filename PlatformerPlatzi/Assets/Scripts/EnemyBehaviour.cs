﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour
{

    Rigidbody2D enemyRb;
    public Animator enemyAnimator;
    ParticleSystem enemyDeathPart;
    AudioSource enemyDeathSound;
    
    float timeBeforeChange;
    public float delay = 2.25f;
    public float speed = 1.0f;
    // Start is called before the first frame update
    void Start()
    {
        enemyRb = this.GetComponent<Rigidbody2D>();
        enemyDeathPart = GameObject.Find("EnemyDeathParticle").GetComponent<ParticleSystem>();
        enemyDeathSound = this.GetComponentInParent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        enemyRb.velocity = Vector2.right * speed;

        if(timeBeforeChange < Time.time){
            speed *= -1;
            timeBeforeChange = Time.time + delay;
        }
        if(speed > 0){
            this.GetComponent<SpriteRenderer>().flipX = false;
        } else if(speed < 0){
            this.GetComponent<SpriteRenderer>().flipX = true;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision){
        if(collision.gameObject.tag == "Player"){
            if(this.transform.position.y < collision.transform.position.y){
                enemyDeathPart.transform.position = transform.position;
                enemyDeathPart.Play();
                enemyAnimator.SetBool("isDead", true);
                speed = 0;
                enemyDeathSound.Play();
            }
        }
    }

    public void DisableEnemy(){
        this.gameObject.SetActive(false);
    }
}
