﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Collectable : MonoBehaviour
{
    public static int collectableQuantity = 0;
    Text CollectableText;
    ParticleSystem collectablePart;

    AudioSource collectableAudio;
    // Start is called before the first frame update
    void Start()
    {
        collectableQuantity = 0;
        CollectableText = GameObject.Find("CollectableQuantityText").GetComponent<Text>();
        collectablePart = GameObject.Find("CollectableParticle").GetComponent<ParticleSystem>();
        collectableAudio = GetComponentInParent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D collider){
        if(collider.tag == "Player"){
            collectablePart.transform.position = transform.position;
            collectablePart.Play();
            gameObject.SetActive(false);
            collectableQuantity++;
            CollectableText.text = collectableQuantity.ToString();
            collectableAudio.Play();
        }
    }
}
