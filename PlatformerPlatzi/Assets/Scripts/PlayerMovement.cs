﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour{

    public Rigidbody2D playerRb;
    public float speed = 0.5f;
    public float jumpSpeed = 300;
    private int jumpCounts;
    public int jumpMax = 2;

    public bool isGrounded = true;

    public Animator playerAnim;
    AudioSource playerJumpSound;
    AudioSource playerMoveSound;
    // Start is called before the first frame update
    void Start()
    {
        playerAnim = this.GetComponent<Animator>();
        playerJumpSound = this.GetComponents<AudioSource>()[0];
        playerMoveSound = this.GetComponents<AudioSource>()[1];
    }

    // Update is called once per frame
    void Update()
    {
        playerRb.velocity = new Vector2(Input.GetAxis("Horizontal") * speed, playerRb.velocity.y);
            if(Input.GetAxis("Horizontal") == 0){
                playerAnim.SetBool("isWalking", false);
            } else if(Input.GetAxis("Horizontal")  == 1){
                this.GetComponent<SpriteRenderer>().flipX = false;
                playerAnim.SetBool("isWalking", true);
            } else if(Input.GetAxis("Horizontal") == -1){
                this.GetComponent<SpriteRenderer>().flipX = true;
                playerAnim.SetBool("isWalking", true);
            }
        
        if(jumpMax > 1){
            if(Input.GetKeyDown(KeyCode.Space) && jumpCounts < jumpMax){
                isGrounded = false;
                playerAnim.SetTrigger("Jump");
                playerJumpSound.Play();
                if(jumpCounts > 1){
                    playerRb.AddForce(Vector2.up * (jumpSpeed/4));
                } else {
                    playerRb.AddForce(Vector2.up * jumpSpeed);
                }
                
                jumpCounts++;
            }
        }else if(jumpMax == 1){
            if(Input.GetKeyDown(KeyCode.Space) && jumpCounts < jumpMax){
                isGrounded = false;
                playerJumpSound.Play();
                playerAnim.SetTrigger("Jump");
                playerRb.AddForce(Vector2.up * jumpSpeed);
                jumpCounts++;
            } 
        }       
    }

    private void OnCollisionEnter2D(Collision2D collision){
        if(collision.gameObject.CompareTag("Floor") || collision.gameObject.CompareTag("FallingFloor")){
            isGrounded = true;
            jumpCounts = 0;
        }
    }
}
