﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingGround : MonoBehaviour
{
    private bool isOnFloor = false;
    private Rigidbody2D fallingGround;
    public bool m_UseGravity = false;
    // Start is called before the first frame update
    void Start()
    {  
        fallingGround = this.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if(isOnFloor){
            fallingGround.bodyType = RigidbodyType2D.Static;
            m_UseGravity = false;
        }
        if(m_UseGravity){
            fallingGround.bodyType = RigidbodyType2D.Dynamic;
        }
    }

    // private void DestroyGameObject(){
    //     Destroy(gameObject);
    // }

    private void OnCollisionEnter2D(Collision2D collision){
        if(collision.gameObject.tag == "Player"){
            // Debug.LogWarning("El jugador entro en contacto");
            // Debug.LogWarning("La posicion del terreno es: " + gameObject.transform.position.y);
            // Debug.LogWarning("La posicion del jugador es: " + collision.transform.position.y);
            //if(transform.position.y < collision.transform.position.y){
                m_UseGravity = true;
            //}
        } 
    }
    
    private void OnCollisionStay2D(Collision2D collision){
        if(collision.gameObject.CompareTag("Floor")){
            isOnFloor = true;
        }
    }
}
